<?php
declare(strict_types=1);

/** @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Address;
use App\Models\Plan;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->address,
        'plan_id' => mt_rand(1, Plan::max('id')),
    ];
});
