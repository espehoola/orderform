<?php
declare(strict_types = 1);

use Illuminate\Database\Seeder;

use App\Models\Address;
use App\Models\Day;
use App\Models\Plan;

/**
 * Class DatabaseSeeder
 * @author Dmitriy Bakhtin <dbahtin@yandex.ru>
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Day::insert(
            [
                ['name' => 'Monday', 'short_name' => 'mo'],
                ['name' => 'Tuesday', 'short_name' => 'tues'],
                ['name' => 'Wednesday', 'short_name' => 'wed'],
                ['name' => 'Thursday', 'short_name' => 'thurs'],
                ['name' => 'Friday', 'short_name' => 'f'],
                ['name' => 'Saturday', 'short_name' => 'sat'],
                ['name' => 'Sunday', 'short_name' => 'sun'],
            ]
        );

        $days = Day::all();


        factory(Plan::class, 10)->create()->each(
            function (Plan $plan) use ($days) {
                $plan->save();
                for ($i = 1; $i <= random_int(3, 7); $i++) {
                    $arrayIds[] = random_int(1, 7);
                }
                $plan->days()->attach($days->find($arrayIds));
            }
        );

        factory(Address::class, 100)->create();
    }
}
