<?php
declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Day
 *
 * @author Dmitriy Bakhtin <dbahtin@yandex.ru>
 * @property int $id
 * @property string $name
 * @property string $short_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day whereShortName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Plan[] $plansList
 */
class Day extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'short_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function plans(): BelongsToMany
    {
        return $this->belongsToMany(Plan::class)->withTimestamps();
    }
}