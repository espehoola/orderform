<?php
declare(strict_types=1);

/** @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Plan;
use Faker\Generator as Faker;

$factory->define(Plan::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->streetSuffix,
        'price' => mt_rand(10, 1000),
    ];
});
