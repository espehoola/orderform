<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Address;

/**
 * Class AddressController
 *
 * @author Dmitriy Bakhtin <dbahtin@yandex.ru>
 */
class AddressController extends Controller
{
    /**
     * @return array
     */
    public function index(): array
    {
        return Address::all()->toArray();
    }
}
