<?php

use App\Models\Plan;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('plans', 'Api\PlanController@index');
Route::get('addresses', 'Api\AddressController@index');
