<?php
declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Day
 *
 * @author Dmitriy Bakhtin <dbahtin@yandex.ru>
 * @property int $id
 * @property string $name
 * @property int $plan_id
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Plan[] $plans
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Day whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Address extends Model
{
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function plans(): BelongsToMany
    {
        return $this->belongsToMany(Plan::class)->withTimestamps();
    }
}
