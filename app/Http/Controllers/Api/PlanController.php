<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Plan;

/**
 * Class PlanController
 *
 * @author Dmitriy Bakhtin <dbahtin@yandex.ru>
 */
class PlanController extends Controller
{
    public function index()
    {
        return Plan::with('days')->get()->toArray();
    }
}
